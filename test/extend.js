$(function(){

  module("View.extend")

  test("Extend protoProps", 2, function(){
    var obj = { counter: 0 }
      , Plus = View.extend({
          plus: function(){
            obj.counter++
          }
        })
      , DoublePlus = Plus.extend({
          plus: function(){
            obj.counter = obj.counter + 2
          }
        })

    var calc = new Plus()
      , calc2 = new DoublePlus()

    calc.plus()
    equal(obj.counter, 1, 'counter should be incremented.')
    calc.plus()
    calc.plus()
    calc2.plus()
    equal(obj.counter, 5, 'counter should be incremented to five.')
  })

  test("Create form protoProps", 1, function(){
    var obj = { counter: 0 }
      , calc = View.create({
          plus: function(){
            obj.counter++
          }
        })
    calc.plus()
    calc.plus()
    equal(obj.counter, 2, 'counter should be 2.')
  })

})
