$(function(){

  module("View.events")

  test("on and trigger", 2, function(){
    var obj = { counter: 0 }
      , view = new View()
    view.on('counter:plus', function(){
      obj.counter++
    })
    view.trigger('counter:plus')
    equal(obj.counter, 1, 'counter should be incremented.')
    view.trigger('counter:plus')
    view.trigger('counter:plus')
    view.trigger('counter:plus')
    view.trigger('counter:plus')
    equal(obj.counter, 5, 'counter should be incremented five times.')
  })

  test("bind two callbacks, unbind only one", 2, function() {
    var obj = { counterA: 0, counterB: 0 }
      , view = new View()
    var callback = function() { obj.counterA += 1 }
    view.on('event', callback)
    view.on('event', function() { obj.counterB += 1 })
    view.trigger('event')
    view.off('event', callback)
    view.trigger('event')
    equal(obj.counterA, 1, 'counterA should have only been incremented once.')
    equal(obj.counterB, 2, 'counterB should have been incremented twice.')
  })
})
