/*
 * XXX:
 *   Split form Backbone.view 0.9.10
 *     without listenTo, stopListening and once
 * */

define('ui/view', [
  'jquery'
], function($) {
  var idCounter = 0

  var _ = {
      result: function(object, property) {
        if (object == null) return null;
        var value = object[property];
        return _.isFunction(value) ? value.call(object) : value;
      }
    , pick: function(obj) {
        var copy = {}
          , ArrayProto = Array.prototype
          , slice = ArrayProto.slice
          , concat = ArrayProto.concat
          , keys = concat.apply(ArrayProto, slice.call(arguments, 1))
        $.each(keys, function(index, key) {
          if (key in obj) {
            copy[key] = obj[key]
          }
        })
        return copy
      }
    , isFunction: $.isFunction
    , extend: $.extend
    , uniqueId: function(prefix) {
        var id = '' + ++idCounter
        return prefix ? prefix + id : id
      }
    , bind: $.proxy
  }

  var events = {
    on: function() {
      $.fn.on.apply(this._events, arguments)
    }
  , off: function() {
      $.fn.off.apply(this._events, arguments)
    }
  , trigger: function() {
      $.fn.trigger.apply(this._events, arguments)
    }
  }

  var extend = function(protoProps, staticProps) {
    var parent = this
      , child = function() {
          return parent.apply(this, arguments)
        }
    _.extend(child, parent, staticProps)
    var Surrogate = function() {
      this.constructor = child
    }
    Surrogate.prototype = parent.prototype
    child.prototype = new Surrogate()
    if (protoProps) {
      _.extend(child.prototype, protoProps)
    }
    return child
  }

  var viewOptions = ['el', 'id', 'attributes', 'className', 'tagName', 'events']
    , delegateEventSplitter = /^(\S+)\s*(.*)$/

  function View(options) {
    this._events = $(this)
    this.cid = _.uniqueId('view')
    this._configure(options || {})
    this._ensureElement()
    this.initialize.apply(this, arguments)
    this.delegateEvents()
  }

  View.extend = extend
  View.create = function(protoProps) {
    var ctor =  View.extend(protoProps)
      , r = new ctor()
    return r
  }

  _.extend(View.prototype, events, {
      tagName: 'div'
    , $: function(selector) {
        return this.$el.find(selector)
      }
    , initialize: function() {}
    , render: function() {
        return this
      }
    , remove: function() {
        this.$el.remove()
        this._events.off()
        return this
      }
    , setElement: function(element, delegate) {
        if (this.$el) this.undelegateEvents()
        this.$el = element instanceof $ ? element : $(element)
        this.el = this.$el[0]
        if (delegate !== false) this.delegateEvents()
        return this
      }
    , delegateEvents: function(events) {
        if (!(events || (events = _.result(this, 'events')))) return
        this.undelegateEvents()
        for (var key in events) {
          var method = events[key]
          if (!_.isFunction(method)) method = this[events[key]]
          if (!method) throw new Error('Method "' + events[key] + '" does not exist')
          var match = key.match(delegateEventSplitter)
          var eventName = match[1], selector = match[2]
          method = _.bind(method, this)
          eventName += '.delegateEvents' + this.cid
          if (selector === '') {
            this.$el.on(eventName, method)
          } else {
            this.$el.on(eventName, selector, method)
          }
        }
      }
    , undelegateEvents: function() {
        this.$el.off('.delegateEvents' + this.cid)
      }
    , _configure: function(options) {
        if (this.options) options = _.extend({}, _.result(this, 'options'), options)
        _.extend(this, _.pick(options, viewOptions))
        this.options = options
      }
    , _ensureElement: function() {
        if (!this.el) {
          var attrs = _.extend({}, _.result(this, 'attributes'))
          if (this.id) attrs.id = _.result(this, 'id')
          if (this.className) attrs['class'] = _.result(this, 'className')
          var $el = $('<' + _.result(this, 'tagName') + '>').attr(attrs)
          this.setElement($el, false)
        } else {
          this.setElement(_.result(this, 'el'), false)
        }
      }
  })

  return View
})
